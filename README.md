# Værsågod Fannypack module for Craft CMS 3

**Jump to docs:**  

- [Jobs](#jobs)
    - [Configuring jobs](#configuring-jobs)
    - [Adding a job](#adding-a-job)
    - [Triggering jobs via cron](#triggering-jobs-via-cron)
- [Console commands](#console-commands)
    - [Running console commands](#running-console-commands)
    - [Bundled console commands](#bundled-console-commands)
        - [Re-save entries](#resave-entries)
        - [Clear sources from Asset caches](#clear-sources-from-asset-caches)
        - [Run pending tasks](#run-pending-tasks)
        - [Clear template caches](#clear-template-caches)
- [Twig filters and functions](#twig-filters-and-functions)
    - [Filters](#filters)
    - [Functions](#functions)
- [Rollbar](#rollbar)
    

## Jobs

Fannypack includes a bespoke queuing system for running automated tasks (we call them _jobs_).

_TODO (maybe) for Craft 3! This is not included yet!_

#### Configuring jobs

* Create a class for your job named `YourPlugin_SomethingSomethingJob.php`, save it to `/yourplugin/jobs`
* Look at the `/vrsg/jobs/Vrsg_ExampleJob.php` to see how the job class should look (trust me, they're simple)
* Copy the `/vrsg/config.php` to your project's config dir – rename the file to `vrsg.php`
* Open the `/config/vrsg.php` file, and add something like the following to the `jobs->types` entry:

`'somethingsomething' => 'YourPlugin_SomethingSomethingJob',`

Done!  

#### Adding a job

`craft()->vrsg_jobs->add('jobhandle', [ 'foo' => 'bar' ]);`

#### Triggering jobs via cron

To run pending jobs as a cronjob, add something like the following command to Forge Scheduler:  

`php /home/forge/example.vaersaagod.no/yiic.php vrsg_jobs runPending > /dev/null 2>&1`

## Console commands

The Fannypack comes w/ a set of nifty console commands.

_TODO for Craft 3! This is not included yet!_

### Running console commands
* Make sure you've got a `yiic.php` file at the root of your project (the plugin comes bundled w/ one). This file basically proxies `yiic` in order to tell Craft which environment we are in, etc.
* From the command line, make sure `pwd` is your project root (i.e. where the `yiic.php` file lives). To execute a command, type something like this:  

`$ php yiic.php commandName actionName --parameter=value`  

### Bundled console commands

#### Re-save entries

**Re-save entries in sections `news` and `events`**
  
`$ php yiic.php vrsg resaveEntries --sections=news,events`

**Re-save disabled entries, too**
  
`$ php yiic.php vrsg resaveEntries --sections=news,events --includeDisabled=true`

**Set a custom chunk size, if you run into memory/timeout issues (default is `100`)**

`$ php yiic.php vrsg resaveEntries --sections=news,events --chunkSize=50`

#### Clear sources from Asset caches

When Craft creates thumbnails for Assets, it'll download and cache original images, which can consume a lot of disk space over time. This command clears out the originals from the Asset cache, but leaves thumbnails alone:  

`$ php yiic.php vrsg clearSourcesFromAssetCaches`

#### Run pending tasks

In some cases, it makes sense to set the `runTasksAutomatically` config setting to `false`, and run this as a cron job:  

`$ php yiic.php vrsg runPendingTasks`

#### Clear template caches

Clears all template caches.  

`$ php yiic.php vrsg clearTemplateCaches`

## Twig filters and functions

### Filters
`cleanWhitespace`  
`removeWhitespace`  
`jsonDecode`  
`ksort`  
`unique`  
`truncateHtml`  
`toFloat`  
`toInt`  
`md5`  
`widont`
`cleanSvg`

### Functions
`cos`  
`sin`  
`pi`  

## Rollbar

To add Rollbar to your project, simply add the `rollbarServerToken` config setting to your `/config/vrsg.php` file. To get a Rollbar server token, visit your project settings on [rollbar.com](https://rollbar.com) and go into the "Project access token" – the key you want is the `post_server_item`.  

Note: The Fannypack only supports server-side Rollbar at this point!  
