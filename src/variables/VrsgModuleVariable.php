<?php
/**
 * Vrsg module for Craft CMS 3.x
 *
 * I can't believe it's not a plugin!
 *
 * @link      https://vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

namespace vaersaagod\vrsg\variables;

use vaersaagod\vrsg\VrsgModule as Vrsg;

/**
 * Vrsg Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.vrsgModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Værsågod
 * @package   VrsgModule
 * @since     1.0.0
 */
class VrsgModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param string $url
     * @param array $params
     * @return mixed
     */
    public function getVideoEmbed(string $url, array $params = [])
    {
        return Vrsg::$instance->vrsgVideoEmbed->getVideoEmbed($url, $params);
    }
}
