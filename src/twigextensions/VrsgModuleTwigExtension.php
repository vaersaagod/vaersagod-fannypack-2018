<?php
/**
 * Vrsg module for Craft CMS 3.x
 *
 * I can't believe it's not a plugin!
 *
 * @link      https://vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

namespace vaersaagod\vrsg\twigextensions;

use vaersaagod\vrsg\VrsgModule;

use Craft;
use craft\helpers\StringHelper;
use craft\helpers\Template;

use enshrined\svgSanitize\Sanitizer;

/**
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators,
 * global variables, and functions. You can even extend the parser itself with
 * node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 *
 * @author    Værsågod
 * @package   VrsgModule
 * @since     1.0.0
 */
class VrsgModuleTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'Fannypack';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('cleanWhitespace', [$this, 'cleanWhitespaceFilter']),
            new \Twig_SimpleFilter('removeWhitespace', [$this, 'removeWhitespaceFilter']),
            new \Twig_SimpleFilter('jsonDecode', [$this, 'jsonDecodeFilter']),
            new \Twig_SimpleFilter('ksort', [$this, 'ksortFilter']),
            new \Twig_SimpleFilter('unique', [$this, 'uniqueFilter']),
            new \Twig_SimpleFilter('truncateHtml', [$this, 'truncateHtmlFilter']),
            new \Twig_SimpleFilter('safeTruncate', [$this, 'safeTruncateFilter']),
            new \Twig_SimpleFilter('toFloat', [$this, 'toFloatFilter']),
            new \Twig_SimpleFilter('toInt', [$this, 'toIntFilter']),
            new \Twig_SimpleFilter('md5', [$this, 'md5Filter']),
            new \Twig_SimpleFilter('camelToSnake', [$this, 'camelToSnakeFilter']),
            new \Twig_SimpleFilter('camelToKebab', [$this, 'camelToKebabFilter']),
            new \Twig_SimpleFilter('widont', [$this, 'widontFilter']),
            new \Twig_SimpleFilter('ascii', [$this, 'asciiFilter']),
            new \Twig_SimpleFilter('sanitizeSvg', [$this, 'sanitizeSvgFilter']),
        ];
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cos', [$this, 'cosFunction']),
            new \Twig_SimpleFunction('sin', [$this, 'sinFunction']),
            new \Twig_SimpleFunction('pi', [$this, 'piFunction']),
        ];
    }

    /**
     * @param $input
     * @param bool $getRaw
     * @return \Twig_Markup
     */
    public function cleanWhitespaceFilter($input, bool $getRaw = true)
    {
        // replace UTF-8 non breaking space with html entity
        $input = preg_replace('~\x{00a0}~siu', '&nbsp;', $input);

        // Remove duplicates
        $input = preg_replace("#(<br\s*/?>\s*)+#", "<br />", $input);

        // Remove orphan
        $input = str_replace("<p><br /></p>", "", $input);

        // Remove leading
        $input = preg_replace("#<p>(<br\s*/?>\s*)+#", "<p>", $input);

        // Remove trailing
        $input = preg_replace("#(<br\s*/?>\s*)+</p>#", "</p>", $input);

        // Remove paragraphs with whitespace and non-breaking space
        $input = preg_replace("#<p>(\s*(&nbsp;)*)*<\/p>#", "", $input);

        return $getRaw ? Template::raw($input) : $input;
    }

    /**
     * @param $input
     * @param bool $getRaw
     * @return \Twig_Markup
     */
    public function removeWhitespaceFilter($input, bool $getRaw = true)
    {
        $input = preg_replace('/\s+/', '', $input);
        return $getRaw ? Template::raw($input) : $input;
    }

    /**
     * @param array $array
     * @param int $sortFlag
     * @return array
     */
    public function ksortFilter(array $array, $sortFlag = SORT_REGULAR)
    {
        ksort($array, $sortFlag);
        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    public function uniqueFilter(array $array)
    {
        return array_unique($array);
    }

    /**
     * @param $val
     * @param bool $assoc
     * @return mixed
     */
    public function jsonDecodeFilter($val, bool $assoc = false)
    {
        return json_decode($val, $assoc);
    }

    /**
     *
     * Stolen from CakePHP, see https://stackoverflow.com/a/2398759/4663184 and
     *
     * @param $text
     * @param int $length
     * @param string $ending
     * @param bool $exact
     * @param bool $considerHtml
     * @return string
     */
    public function truncateHtmlFilter($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false)
    {
        if (is_array($ending)) {
            extract($ending);
        }
        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';
            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);
            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0, $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }

        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }

        return Template::raw($truncate);
    }

    /**
     * @param $value
     * @param int $length
     * @param string $substring
     * @return string
     */
    public function safeTruncateFilter($value, int $length, string $substring = '')
    {
        return StringHelper::safeTruncate($value, $length, $substring);
    }

    /**
     * @param $input
     * @return float
     */
    public function toFloatFilter($input)
    {
        return (float)$this->removeWhitespaceFilter(strip_tags($input), false);
    }

    /**
     * @param $input
     * @return int
     */
    public function toIntFilter($input)
    {
        return (int)$this->removeWhitespaceFilter(strip_tags($input), false);
    }

    /**
     * @param $input
     * @return string
     */
    public function md5Filter($input)
    {
        return md5($input);
    }

    /**
     * @param $input
     * @return string
     */
    public function camelToSnakeFilter($input)
    {
        return $this->convertCamelCase($input, '_');
    }

    /**
     * @param $input
     * @return string
     */
    public function camelToKebabFilter($input)
    {
        return $this->convertCamelCase($input, '-');
    }

    /**
     * @param $input
     * @return mixed
     */
    public function widontFilter($input)
    {
        $expression = "/([^\s])\s+(((<(a|span|i|b|em|strong|acronym|caps|sub|sup|abbr|big|small|code|cite|tt)[^>]*>)*\s*[^\s<>]+)(<\/(a|span|i|b|em|strong|acronym|caps|sub|sup|abbr|big|small|code|cite|tt)>)*[^\s<>]*\s*(<\/(p|h[1-6]|li)>|$))/i";
        return Template::raw(preg_replace($expression, '$1&nbsp;$2', $input));
    }

    /**
     * @param $value
     * @return string
     */
    public function asciiFilter($value)
    {
        return StringHelper::toAscii($value);
    }

    /**
     * @param $svg
     * @param bool $namespace
     * @return \Twig_Markup
     */
    public function sanitizeSvgFilter($svg, bool $namespace = true)
    {

        if (!$svg || !\strlen($svg)) {
            return Template::raw('');
        }

        try {

            $svg = (new Sanitizer())->sanitize($svg);

            // Remove the XML declaration
            $svg = \preg_replace('/<\?xml.*?\?>/', '', $svg);

            // Namespace class names and IDs
            if ($namespace && (\strpos($svg, 'id=') !== false || strpos($svg, 'class=') !== false)) {
                $ns = StringHelper::randomStringWithChars('abcdefghijklmnopqrstuvwxyz', 10) . '-';
                $ids = [];
                $classes = [];
                $svg = \preg_replace_callback('/\bid=([\'"])([^\'"]+)\\1/i', function($matches) use ($ns, &$ids) {
                    $ids[] = $matches[2];
                    return "id={$matches[1]}{$ns}{$matches[2]}{$matches[1]}";
                }, $svg);
                $svg = \preg_replace_callback('/\bclass=([\'"])([^\'"]+)\\1/i', function($matches) use ($ns, &$classes) {
                    $newClasses = [];
                    foreach (preg_split('/\s+/', $matches[2]) as $class) {
                        $classes[] = $class;
                        $newClasses[] = $ns . $class;
                    }
                    return 'class=' . $matches[1] . implode(' ', $newClasses) . $matches[1];
                }, $svg);
                foreach ($ids as $id) {
                    $quotedId = preg_quote($id, '\\');
                    $svg = preg_replace("/#{$quotedId}\b(?!\-)/", "#{$ns}{$id}", $svg);
                }
                foreach ($classes as $class) {
                    $quotedClass = preg_quote($class, '\\');
                    $svg = preg_replace("/\.{$quotedClass}\b(?!\-)/", ".{$ns}{$class}", $svg);
                }
            }

        } catch (\Throwable $e) {
            return Template::raw('');
        }

        return Template::raw($svg);
    }

    /**
     * @param $value
     * @return float
     */
    public function cosFunction($value)
    {
        return cos($value);
    }

    /**
     * @param $value
     * @return float
     */
    public function sinFunction($value)
    {
        return sin($value);
    }

    /**
     * @return float
     */
    public function piFunction()
    {
        return pi();
    }

    /*
     * Private methods
     *
     */
    /**
     * @param $input
     * @param $delimiter
     * @return string
     */
    private function convertCamelCase($input, $delimiter)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('-', $ret);
    }

}
