<?php
/**
 * Vrsg module for Craft CMS 3.x
 *
 * I can't believe it's not a plugin!
 *
 * @link      https://vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

/**
 * Vrsg en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('vrsg-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Værsågod
 * @package   VrsgModule
 * @since     1.0.0
 */
return [
    'Vrsg plugin loaded' => 'Vrsg plugin loaded',
];
