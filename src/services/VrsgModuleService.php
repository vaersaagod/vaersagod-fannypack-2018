<?php
/**
 * Vrsg module for Craft CMS 3.x
 *
 * I can't believe it's not a plugin!
 *
 * @link      https://vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

namespace vaersaagod\vrsg\services;

use vaersaagod\vrsg\VrsgModule;

use Craft;
use craft\base\Component;

/**
 * VrsgModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Værsågod
 * @package   VrsgModule
 * @since     1.0.0
 */
class VrsgModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     VrsgModule::$instance->vrsgModuleService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
